const colors=require('colors');
const dotenv=require('dotenv');
const express = require("express");
const mongoose = require("mongoose");

const app = express();

dotenv.config({
    path:'./config/config.env'
});
app.use(express.json({}));
app.use(express.json({
    extended:true
}));

//create db connection
const PORT=process.env.PORT || 5000
mongoose.connect(process.env.DATABASE_URL, { useUnifiedTopology: true, useNewUrlParser: true })
  .then(() => {
    console.log("Successfully connected to database".green.underline.bold);
  })
  .catch((error) => {
    console.log("database connection failed. exiting now...".red.underline.bold);
    console.error(error);
    process.exit(1);
  });
/**create routers for application */
const employeesRouter = require('./routers/employees');
const usersRouter = require('./routers/users');

app.use("/api/v1/employee", employeesRouter);
app.use("/api/v1/user", usersRouter);
/**End of code */

app.listen(PORT,console.log(`Server iS Running On Port ${PORT}`.red.underline.bold));

