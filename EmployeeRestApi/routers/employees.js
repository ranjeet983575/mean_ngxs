const express = require("express");
const Employee = require("../models/EmployeeModel");
const auth=require('../middleware/userjwt');

const router = express.Router();
let cors = require('cors');
router.use(cors({origin: '*'}));

//Getting all Employee list
router.get("/", auth,async (req, res) => {
        try {
                const employees = await Employee.find();
                await res.json(employees);
        } catch (error) {
                res.status(500).json({ message: error.message });
        }
});

//Getting one employee detail
router.get("/:id",auth,findEmployee, (req, res) => {
        res.status(200).json(res.employee);
});

//Creating one employee
router.post("/",  auth,async (req, res) => {
        const employee = new Employee({
                name: req.body.name,
                contact: req.body.contact,
                dob:req.body.dob,
                bg:req.body.bg,
                gender:req.body.gender,
                address:req.body.address
        });
        try {
                const newEmployee = await employee.save();
                res.status(201).json(newEmployee);
        }
        catch (error) {
                res.status(400).json({ message: error.message });
        }
});

//Updating one employee
router.put("/:id",  auth,findEmployee, async (req, res) => {
        if (req.body.name != null) {
                res.employee.name = req.body.name;
        }
        if (req.body.contact != null) {
                res.employee.contact = req.body.contact;
        }
        if (req.body.dob != null) {
                res.employee.dob = req.body.dob;
        }
        if (req.body.bg != null) {
                res.employee.bg = req.body.bg;
        }
        if (req.body.gender != null) {
                res.employee.gender = req.body.gender;
        }
        if (req.body.address != null) {
                res.employee.address = req.body.address;
        }
        try {
                const updateEmployee = await res.employee.save();
                res.status(200).json(updateEmployee);
        } catch (error) {
                res.status(400).json({ message: error.message });
        }
});

//Deleting one
router.delete("/:id",  auth,findEmployee, async (req, res) => {
        try {
                await res.employee.remove();
                res.status(200).json({ message: "success" });
        } catch (error) {
                return res.status(500).json({ message: error.message });
        }
});


async function findEmployee(req, res, next) {
        let employee;
        try {
                employee = await Employee.findById(req.params.id);
                if (employee == null) {
                        return res.status(400).json({ message: 'Con not found Employee' });
                }
        } catch (error) {
                return res.status(500).json({ message: error.message });
        }
        res.employee = employee;
        next();
}

module.exports = router;
