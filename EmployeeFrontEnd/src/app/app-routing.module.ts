import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import {AllEmployeeComponent} from './pages/all-employee/all-employee.component';
import {CreateEmployeeComponent} from './pages/create-employee/create-employee.component';
import { ViewEmployeeComponent } from './pages/view-employee/view-employee.component';
import { createComponent } from '@angular/compiler/src/core';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'employee', children: [
      { path: '', component: AllEmployeeComponent },
      { path: ':id', component: ViewEmployeeComponent },
      { path: 'create', component: CreateEmployeeComponent },
      { path: 'update/:id', component: CreateEmployeeComponent },
    ],    
  },
  
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
