import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AllEmployeeComponent } from './pages/all-employee/all-employee.component';
import { CreateEmployeeComponent } from './pages/create-employee/create-employee.component';
import { ViewEmployeeComponent } from './pages/view-employee/view-employee.component';
import { LoginComponent } from './pages/login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './config/TokenInterceptor';
import { EmployeeService } from './services/employee.service';
import { UserService } from './services/user.service';

@NgModule({
  declarations: [
    AppComponent,
    AllEmployeeComponent,
    CreateEmployeeComponent,
    ViewEmployeeComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ EmployeeService,UserService,{ provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
