import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-all-employee',
  templateUrl: './all-employee.component.html',
  styleUrls: ['./all-employee.component.scss']
})
export class AllEmployeeComponent implements OnInit {

  public employees=[];

  constructor(private service: EmployeeService) { }

  ngOnInit(): void {
    this.service.getAllEmployees().subscribe((res:any)=>{
        this.employees=res;    
    });
  }

}
