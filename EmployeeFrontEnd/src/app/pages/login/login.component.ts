import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginModel } from 'src/app/models/LoginModel';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private service: UserService,private router:Router) { }

  ngOnInit(): void {
  }

  public login():void{

    let model=new LoginModel();
    model.email="ranjeet@gmail.com";
    model.password="Ranjeet@123#";

    this.service.login(model).subscribe((res:any)=>{
       if(res)
       {
        localStorage.setItem("token",res.token);
        this.router.navigate(['/employee']) 
       }
        
    });



  }

}
