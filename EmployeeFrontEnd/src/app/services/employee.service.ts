import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }
  
  
  public getAllEmployees():any {
    return this.http.get(environment.host+"/employee");
  }

  public getEmployees(id:string):any {
    return this.http.get(environment.host+"/employee/"+id);
  }




}
